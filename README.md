# SimPresence

SimPresence is an application for Microsoft Flight Simulator that shows information about your current flight in your Discord status.

## Todo
- [x] Basic Info (Altitude, Airspeed, Location)
- [ ] More Info
- [ ] Settings File
- [ ] Nicer UI

## Installation
1. Download latest file from the [releases](https://gitlab.com/flerouwu/simpresence/-/releases) page.
