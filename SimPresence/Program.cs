﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CTrue.FsConnect;
using DiscordRPC;
using Microsoft.FlightSimulator.SimConnect;

namespace SimPresence
{
    public enum Requests
    {
        PlaneInfoRequest = 0
    }

    //
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct PlaneInfoResponse
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string Title;

        #region Position
        [SimVar(UnitId = FsUnit.Degree)]
        public double PlaneLatitude;

        [SimVar(UnitId = FsUnit.Degree)]
        public double PlaneLongitude;
        #endregion

        #region Altitude, Heading, Speed
        [SimVar(UnitId = FsUnit.Feet)]
        public double PlaneAltitude;

        [SimVar(UnitId = FsUnit.Degree)]
        public double PlaneHeadingDegreesTrue;

        [SimVar(NameId = FsSimVar.AirspeedTrue, UnitId = FsUnit.MeterPerSecond)]
        public double AirspeedTrueInMeterPerSecond;

        [SimVar(NameId = FsSimVar.AirspeedTrue, UnitId = FsUnit.Knot)]
        public double AirspeedTrueInKnot;
        #endregion

        #region Information
        [SimVar(NameId = FsSimVar.AtcModel)]
        public int AtcModel;

        [SimVar(NameId = FsSimVar.AtcId)]
        public int AtcId;

        [SimVar(NameId = FsSimVar.AtcHeavy)]
        public bool IsHeavy;

        [SimVar(NameId = FsSimVar.AtcFlightNumber)]
        public int AtcFlightNumber;
        #endregion
    }

    public class Program
    {
        // Presence
        private static readonly DiscordRpcClient client = new DiscordRpcClient("1118304481260621845");
        private static readonly RichPresence presence = new RichPresence()
        {
            Assets = new Assets {
                LargeImageKey = "msfs"
            },

            Buttons = new Button[] {
                new Button()
                {
                    Label = "SimPresence by Flero",
                    Url = "https://gitlab.com/flerouwu/simpresence"
                }
            },

            Timestamps = Timestamps.Now
        };

        public static void Main(string[] args)
        {
            Console.WriteLine("SimPresence v1.0 | Developed by Flero");
            Console.WriteLine("-------------------------------------");

            string hostname = "localhost";
            uint port = 500;

            // Also supports "somehostname 1234"
            if (args.Length == 2)
            {
                hostname = args[0];
                port = uint.Parse(args[1]);
            }

            FsConnect connection = new FsConnect();

            // Specify where the SimConnect.cfg should be written to
            connection.SimConnectFileLocation = SimConnectFileLocation.Local;

            // Creates a SimConnect.cfg and connect to Flight Simulator using this configuration
            connection.Connect("SimPresence", hostname, port, SimConnectProtocol.Ipv4);

            // Other alternatives, use existing SimConnect.cfg and specify config index:
            // connection.Connect(1);
            // or
            // connection.Connect();

            connection.FsDataReceived += HandleReceivedFsData;

            // Connect to RPC
            client.Initialize();

            // Loop
            Thread thread = new Thread(delegate() { RunRPC(connection); });
            thread.Start();

            Console.WriteLine("Press [ESC] to exit.");
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                if (key.Key == ConsoleKey.Escape) break;
            }

            Console.WriteLine("Exiting...");
            thread.Abort();
            connection.Disconnect();
        }

        private static void RunRPC(FsConnect connection)
        {
            // Consult the SDK for valid sim variable names, units and whether they can be written to.
            int planeInfoDefinitionId = connection.RegisterDataDefinition<PlaneInfoResponse>();

            while (true)
            {
                connection.RequestData((int)Requests.PlaneInfoRequest, planeInfoDefinitionId);
                Thread.Sleep(1000);
            }
        }

        private static void HandleReceivedFsData(object sender, FsDataReceivedEventArgs e)
        {
            if (e.RequestId == (uint)Requests.PlaneInfoRequest)
            {
                PlaneInfoResponse r = (PlaneInfoResponse)e.Data.FirstOrDefault();
                //Console.WriteLine($"{r.PlaneLatitude:F4} {r.PlaneLongitude:F4} {r.PlaneAltitude:F1}ft {r.PlaneHeadingDegreesTrue:F1}deg {r.AirspeedTrueInMeterPerSecond:F0}m/s {r.AirspeedTrueInKnot:F0}kt");

                // Plane Information
                /*client.SetPresence(presence
                    .WithState($"Flight {r.AtcFlightNumber} | {r.AtcId}")
                    .WithDetails($"Aircraft {r.AtcModel} {(r.IsHeavy ? "(HEAVY)" : "")}"));*/

                // Sleep before changing
                //Thread.Sleep(5000);

                // Feet, Knots, Longitude, Latitude
                client.SetPresence(presence
                    .WithDetails($"{r.PlaneAltitude:F0} feet - {r.AirspeedTrueInKnot:F0} knots")
                    .WithState($"{r.PlaneLatitude:F4} lat - {r.PlaneLongitude:F4} long"));
            }
        }
    }
}